# sq_scene

Usage parameters:
- `-ec` To use extended convexity
- `-sc` To use the sanity criterion
- `-tvoxel` To use single camera transform
- `-refine` To use supervoxel refinement
- `-v <number from 0 to 1>` For voxel resolution
- `-s <number from 0 to 1>` For seed resolution
- `-c <number from 0 to 1>` For color importance
- `-z <number from 0 to 1>` For spatial importance
- `-n <number from 0 to 1>` For normal importance
- `-smooth <any positive number>` For min number of segment size
- `-minPoints <any positive number>` For min number of points in a segmented object
- `-maxPoints <any positive number>` for max number of points in a segmented object

Example:
```
./segmented_scene -offline -ct 50 -smooth 1000000 -ec -st 1 -s 1 -minPoints 100 -maxPoints 250000
```
It will ask you to enter the path of your pcd file, which you can copy and paste in the terminal.

### Instructions
```
cd sq_scene
mkdir build && cd build
cmake ..
make -j16
```
